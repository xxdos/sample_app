Rails.application.routes.draw do
  get 'users/new'

  root 'static_pages#home'
  #home es a a que vista predeterminada mandara al abrir la página. Vamos a testear static_pages
  #get 'static_pages/home' Esta la comento para recordar que estuvo ahí antes

  get '/help',  to: 'static_pages#help' # , as :'helf'
                                          # ^ Como hacer esto? y que funcione?
  get '/about', to: 'static_pages#about'
  
  get '/contact', to: 'static_pages#contact'
  #Si comento la siguiente linea, compruebo los ejercicios de la sección 5.4.2
  get  '/signup',  to: 'users#new'
  
  

  # root 'application#hello'

   
end
