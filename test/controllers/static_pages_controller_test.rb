require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "Ruby on Rails Tutorial Sample App"
  end
  
  test "should get home" do
    get root_path
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "should get help" do
    get help_path
    assert_response :success
    assert_select "title", "Help | #{@base_title}"
  end
  
  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "About | #{@base_title}"
  end
  
  test "should get contact" do
    get contact_path
    assert_response:success
    assert_select "title", "Contact | #{@base_title}"
  end
  #Ejercicio 3.4.4.1 que testea que el root devuelve a url root
    test "should get root" do
    get root_url
    #Ejercicio 3.4.4.2 a ver si devueve red en caso de comentar la linea de get, sale bien.
    assert_response :success
  end
end
