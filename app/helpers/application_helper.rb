module ApplicationHelper
   # Returns fu tite on a per-page basis / Devuelve e titulo completo en base a un titulo por página
  def full_title(page_title = '')
    base_title = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
