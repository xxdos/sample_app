# Ruby on Rails Tutorial sample application
# Copypaste guarro de readme, con cosas comentadas

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

$ bundle install --without production

Next, migrate the database:

$ rails db:migrate

Finally, run the test suite to verify that everything is working correctly:

$ rails test

If the test suite passes, you'll be ready to run the app in a local server:

$ rails server

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).


# Anotaciones propias
Para instalar gemas, paquetes, extensiones, y weas hacer esto en orden:
1-en a raiz de la carpeta que se crea e projecto a hacer un "rails new proyecto", buscar el archivo gemfile y modificar con algo tal que así:

source 'https://rubygems.org'

gem 'rails',        '5.1.4'

group :development, :test do
  gem 'sqlite3', '1.3.13'
  gem 'byebug',  '9.0.6', platform: :mri
end

group :development do
  gem 'web-console',           '3.5.1'
  gem 'listen',                '3.1.5'
  ...
end

group :test do
  gem 'rails-controller-testing', '1.0.2'
  ...
end

group :production do
  gem 'pg', '0.18.4'
end

Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

Algunas no tienen sentido aparente, pero ya las iré viendo
2- tocar los bundle una vez terminado con el gemfile
Si no tira una, tirar el update antes y/o despues del install (Por ahora lo hacemos con el without production)
$ bundle install --without production
$ bundle update

Y eso.